class ApplicationMailer < ActionMailer::Base
  default from: "rumargibs@gmail.com"
  layout "mailer"
end
